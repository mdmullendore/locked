import React, { useState } from 'react';
import { Switch, Route } from 'react-router';
import CssBaseline from '@material-ui/core/CssBaseline';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import routes from './constants/routes';
import App from './containers/App';
import HomePage from './containers/HomePage';
import CounterPage from './containers/CounterPage';

export default () => {
  const [theme] = useState({
    palette: {
      type: 'dark'
    }
  });

  const darkTheme = createMuiTheme(theme);

  return (
    <MuiThemeProvider theme={darkTheme}>
      <CssBaseline />
      <App>
        <Switch>
          <Route path={routes.counter} component={CounterPage} />
          <Route path={routes.home} component={HomePage} />
        </Switch>
      </App>
    </MuiThemeProvider>
  );
};
