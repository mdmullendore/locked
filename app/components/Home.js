// @flow
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Button from '@material-ui/core/Button';
import routes from '../constants/routes';

type Props = {};

export default class Home extends Component<Props> {
  props: Props;

  render() {
    return (
      <div data-tid="container">
        <h1>Locked Password Manager</h1>
        <Link to={routes.counter}>Counter</Link>
        <Button
          variant="contained"
          color="secondary"
          href={`#${routes.counter}`}
        >
          Create User
        </Button>
      </div>
    );
  }
}
